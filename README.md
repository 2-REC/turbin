# Turbin

Short movie project with friends.<br>
Mockumentary about unemployment solutions in Belgium.<br>

The files in this repository are the infographics used for the "special effects" (overlays).<br>

The final version is viewable [here](https://www.youtube.com/watch?v=933DBNO4m2c&t).<br>
